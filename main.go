package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/go-redis/redis"
	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/streadway/amqp"
)

func envVariable(key string) string {

	return os.Getenv(key)

}

func main() {

	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	db, err := sql.Open("mysql", "05d92b6b7ccb46e5baea32099b0a8e00:w8jfy6laooi5472e@tcp(2da2a566-ebd4-4f77-846e-5e27add9b5f8.mysql.service.internal:3306)/service_instance_db")

	// if there is an error opening the connection, handle it
	if err != nil {
		panic(err.Error())
	}

	// defer the close till after the main function has finished executing
	defer db.Close()

	var firstname string
	var lastname string
	sqlStatement := "SELECT firstname, lastname FROM my_users"
	row := db.QueryRow(sqlStatement)
	err = row.Scan(&firstname, &lastname)
	if err != nil {
		if err == sql.ErrNoRows {
			fmt.Println("Zero rows found")
		} else {
			panic(err)
		}
	}

	var results string = "Hello " + firstname + " " + lastname + "!!!"

	// redis test
	client := redis.NewClient(&redis.Options{
		Addr:     "q-s0.redis-instance.services.service-instance-3b58bc50-5c52-4b44-9692-472ef6e4bbd0.bosh:6379",
		Password: "E0bo4M5nSm39wBEMtPLcBkVdUiepAh",
	})

	err = client.Set("greeting", "Hello, this is taken from redis", 0).Err()
	// if there has been an error setting the value

	// handle the error
	if err != nil {
		fmt.Println(err)
	}

	val, err := client.Get("greeting").Result()
	results += "<br/>" + val
	// redis test end

	e.GET("/", func(c echo.Context) error {
		return c.HTML(http.StatusOK, results)
	})

	e.GET("/ping", func(c echo.Context) error {
		return c.JSON(http.StatusOK, struct{ Status string }{Status: "OK"})
	})

	e.GET("/send", func(c echo.Context) error {
		rabbitMQsend()
		return c.JSON(http.StatusOK, "Message sent")
	})

	httpPort := os.Getenv("HTTP_PORT")
	if httpPort == "" {
		httpPort = "8080"
	}

	e.Logger.Fatal(e.Start(":" + httpPort))

}

func rabbitMQsend() {
	conn, err := amqp.Dial("amqp://3a12d5fe-be32-4487-afd8-b5955ffae6de:ZuY38ZJzLI6-gjVsbleO5K7Y@q-s0.rabbitmq-server.services.service-instance-6bf41461-2cac-4021-8608-a395f8c6c523.bosh/6bf41461-2cac-4021-8608-a395f8c6c523")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"hello", // name
		false,   // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	failOnError(err, "Failed to declare a queue")

	body := "Hello, this is sent from tas-test-golang"
	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(body),
		})
	failOnError(err, "Failed to publish a message")
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}
